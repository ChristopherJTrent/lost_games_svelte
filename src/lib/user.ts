export type User = {
    username:string,
    display_name:string,
    iconURL:string
}
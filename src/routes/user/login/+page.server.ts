import {FORBIDDEN, INVALID_REQUEST} from '$lib/constants.js'
import client from '$lib/database'
import {fail} from '@sveltejs/kit'
import bcrypt from 'bcrypt'
export const actions = {
	default: async({request}) => {
		const data = await request.formData()

		if (data === null) { return fail(INVALID_REQUEST, {error: 'Please fill in the login form.'}) }

		const username = data.get('username')
		const password = data.get('password')
		if (username === null
			|| password === null
			|| username instanceof File
			|| password instanceof File
			|| username ===''
			|| password ==='') {
			return fail(INVALID_REQUEST,
				{
					error: 'Received incomplete or invalid form data. Missing fields:'
                    + username === null || username instanceof File ? 'Username ' : ''
                    + password === null || password instanceof File ? 'Password'  : '',
				}
			)
		}
		const user = await client.user.findUnique({where: {name: username}})
		if (user === null || user === undefined) {
			return fail(FORBIDDEN, {error: 'You cannot login without being registered.'})
		}
		if (await bcrypt.compare(password, user.passwordHash)) {
			//TODO: Finish login logic.
		}
	},
}
